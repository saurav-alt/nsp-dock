const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");


const sequelize = require("./util/database");
const User = require("./models/users");

const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json())

app.use(cors());


app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/dev", require("./routes/dev")); //comment
app.use("/users", require("./routes/users"));

(async () => {
  try {
    await sequelize.sync(
      { force: false } //Reset db every time
    );
    console.log("test");
    app.listen(process.env.EXTERNAL_PORT);
  } catch (error) {
    console.log(error);
  }
})();
