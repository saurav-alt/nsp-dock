import React from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";



class AddEditForm extends React.Component {
  state = {
    username: "",
    email: "",
    password: "",
  };

  onChange = (id) => {
    this.setState({ [id.target.name]: id.target.value });
  };

  submitFormAdd = (e) => {
    e.preventDefault();
    fetch("http://localhost:3001/users", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
      }),
    })
      .then((response) => response.json())
      .then((item) => {
        if (Array.isArray(item)) {
          this.props.addItemToState(item[0]);
          this.props.toggle();
        } else {
          console.log("failure");
        }
      })
      .catch((err) => console.log(err));
      window.location.reload();
  };

  submitFormEdit = (e) => {
    e.preventDefault();

    console.log(e.target);
    fetch(`http://localhost:3001/users/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: this.state.id,
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
      }),
    })
      .then((response) => response.json())
      .then((item) => {
        if (Array.isArray(item)) {
          // console.log(item[0])
          this.props.updateState(item[0]);
          this.props.toggle();
          
        } else {
          console.log("failure");
        }
      })
      .catch((err) => console.log(err));
      window.location.reload();
  };

  componentDidMount() {
    // if item exists, populate the state with proper data
    if (this.props.item) {
      const { id, username, email, password } = this.props.item;
      this.setState({ id, username, email, password });
    }
  }

  render() {
    return (
      <Form
        onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}
      >
        <FormGroup>
          <Label for="username">UserName</Label>
          <Input
            type="text"
            name="username"
            id="username"
            onChange={this.onChange}
            value={this.state.username === null ? "" : this.state.username}
          />
        </FormGroup>
        <FormGroup>
          <Label for="email">email</Label>
          <Input
            type="email"
            name="email"
            id="email"
            onChange={this.onChange}
            value={this.state.email === null ? "" : this.state.email}
          />
        </FormGroup>
        <FormGroup>
          <Label for="password">password</Label>
          <Input
            type="text"
            name="password"
            id="password"
            onChange={this.onChange}
            value={this.state.password === null ? "" : this.state.password}
          />
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}



export default AddEditForm;
